const ghPages = require('gh-pages');

try {
  ghPages.publish('.', {
    branch: 'dist',
    src: [
      'build/**/*',
      'log4js.json',
      '.env.example',
      'ormconfig.example.json',
      'package.json',
      'tsconfig.json',
      'yarn.lock',
    ],
  }, () => {});
} catch (err) {
  console.error(err);
}
