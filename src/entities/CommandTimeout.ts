import {
  BaseEntity,
  Column,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('command_timeouts')
export default class CommandTimeoutEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public command: string;

  @Column({
    type: 'bigint',
  })
  public userId: string;

  @Column({
    type: 'bigint',
  })
  public usedAt: string;
}
