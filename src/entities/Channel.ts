import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import GuildEntity from './Guild';

@Entity('channels')
export default class ChannelEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public externalId: string;

  @Column()
  public guildId: number;

  @Column({
    default: 100073,
  })
  public filterId: number;

  @Column({
    default: 0,
  })
  public lastPostId: number;

  @Column({
    type: 'text',
  })
  public q: string;

  @ManyToOne(() => GuildEntity, guild => guild.channels)
  guild: GuildEntity;
}
