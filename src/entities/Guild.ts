import {
  BaseEntity,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import ChannelEntity from './Channel';

@Entity('guilds')
export default class GuildEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public externalId: string;

  @OneToMany(() => ChannelEntity, channel => channel.guild)
  channels: ChannelEntity[];
}
