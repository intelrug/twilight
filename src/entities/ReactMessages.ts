import {
  BaseEntity,
  Column,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('react_messages')
export default class ReactMessagesEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({
    type: 'bigint',
  })
  public channelId: string;

  @Column({
    type: 'bigint',
  })
  public messageId: string;
}
