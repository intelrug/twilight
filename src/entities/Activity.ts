import {
  BaseEntity,
  Column,
  Entity, Index,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('activity')
export default class ActivityEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public guildId: string;

  @Column()
  public channelId: string;

  @Column()
  public messageId: string;

  @Column()
  public userId: string;

  @Column()
  public user: string;

  @Column({
    type: 'bigint',
  })
  public timestamp: number;

  @Index({ fulltext: true })
  @Column({
    type: 'text',
    nullable: true,
  })
  public content: string;
}
