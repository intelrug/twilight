import { Message, TextChannel } from 'discord.js';
import * as fs from 'fs';
import { configure, getLogger, Logger } from 'log4js';
import { CommandoClient } from 'discord.js-commando';
import { createConnection } from 'typeorm';
import * as path from 'path';
import { bootstrap } from 'global-agent';
import Activity from './lib/activity';
import Posting from './lib/posting';
import PrincessCommand from './lib/princess';
import Radio from './lib/radio';
import ReactMessages from './lib/react';

bootstrap();

(async () => {
  let client: CommandoClient;
  let logger: Logger;

  async function setupLogger() {
    try {
      fs.mkdirSync('../log');
    } catch (e) {
      if (e.code !== 'EEXIST') {
        console.error('Could not set up log directory, error was: ', e);
        process.exit(1);
      }
    }

    configure('./log4js.json');
    logger = getLogger('INDEX');
  }

  async function setupDiscord() {
    client = new CommandoClient({
      commandPrefix: '!',
      owner: ['254863702115745794', '140503645337681921'],
      unknownCommandResponse: false,
    });

    client.registry
      .registerDefaultTypes()
      .registerDefaultGroups()
      .registerDefaultCommands()
      .registerGroups([
        ['derp', 'Posting arts from derpibooru'],
        ['react', 'Creating react messages'],
        ['activity', 'Getting activity statistics'],
        ['roll', 'Roll dice'],
      ])
      .registerCommandsIn(path.join(__dirname, 'commands'));

    client.on('ready', () => {
      logger.info(`Logged in as ${client.user.tag}!`);
    });

    client.on('error', error => {
      logger.error(error);
    });
    logger.info('Logging in to Discord...');

    return client.login(process.env.DISCORD_TOKEN);
  }

  async function say(message: Message) {
    if (message.channel.id === process.env.SAY_CHANNEL_ID) {
      const match = message.content.match(/^ *(<#(\d+)> *)?(.*)/);
      if (match) {
        const channelId = match[2] || process.env.DEFAULT_TALK_CHANNEL_ID;
        const ch = message.guild.channels.get(channelId);
        if (ch) {
          (ch as TextChannel).send(match[3]);
        }
      }
    } else if (message.channel.id === process.env.PRINCESS_CHANNEL_ID && message.isMentioned(client.user)) {
      await PrincessCommand.run(message);
    }
  }

  async function run() {
    await ReactMessages.init(client);
    await Posting.init(client);
    await Activity.init(client);
    client.on('message', message => {
      Activity.onMessage(message);
      say(message);
    });
    client.on('messageDelete', Activity.onMessageDelete);
    client.on('guildMemberAdd', Activity.onGuildMemberAdd);
    client.on('guildMemberRemove', Activity.onGuildMemberRemove);
    client.on('guildMemberUpdate', Activity.onGuildMemberUpdate);
    client.on('messageReactionAdd', ReactMessages.onReactionAdd);
    client.on('messageReactionRemove', ReactMessages.onReactionRemove);
    setInterval(() => Radio.getLiveInfo(client), 10000);
  }

  await createConnection();
  await setupLogger();
  logger.info('Starting the bot...');
  try {
    await setupDiscord();
    logger.info('Bot is running!');
    await run();
  } catch (e) {
    logger.error(e);
  }
})();
