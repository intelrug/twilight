import { CommandoClient, CommandMessage, Command } from 'discord.js-commando';
import { Message } from 'discord.js';
import ReactMessages from '../../lib/react';

export default class React extends Command {
  constructor(client: CommandoClient) {
    super(client, {
      name: 'react',
      group: 'react',
      memberName: 'react',
      description: 'Create react message',
      guildOnly: true,
      ownerOnly: true,
      args: [
        {
          key: 'channel',
          prompt: 'Which channel do you want to edit?',
          type: 'channel',
        },
        {
          key: 'value',
          prompt: 'Text',
          type: 'string',
        },
      ],
    });
  }

  async run(msg: CommandMessage, { channel, value }) {
    const message = (await channel.send(value)) as Message;
    await ReactMessages.create(message);
    await msg.delete();
    return null;
  }
}
