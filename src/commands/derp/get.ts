import { CommandoClient, CommandMessage, Command } from 'discord.js-commando';
import Posting from '../../lib/posting';

export default class DerpGet extends Command {
  constructor(client: CommandoClient) {
    super(client, {
      name: 'derp-get',
      group: 'derp',
      memberName: 'derp-get',
      description: 'Get search queries for derpibooru posting',
      guildOnly: true,
      ownerOnly: true,
    });
  }

  async run(msg: CommandMessage) {
    return Posting.get(msg);
  }
}
