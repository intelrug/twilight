import { CommandoClient, CommandMessage, Command } from 'discord.js-commando';
import Posting from '../../lib/posting';

export default class DerpSet extends Command {
  constructor(client: CommandoClient) {
    super(client, {
      name: 'derp-set',
      group: 'derp',
      memberName: 'derp-set',
      description: 'Edit search queries for derpibooru posting',
      guildOnly: true,
      ownerOnly: true,
      args: [
        {
          key: 'channel',
          prompt: 'Which channel do you want to edit?',
          type: 'channel',
        },
        {
          key: 'value',
          prompt: 'Specify filter id or search query',
          type: 'string',
        },
      ],
    });
  }

  async run(msg: CommandMessage, { channel, value }) {
    return Posting.set(msg, channel.id, value);
  }
}
