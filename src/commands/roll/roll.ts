import { CommandoClient, CommandMessage, Command } from 'discord.js-commando';
import Utils from '../../lib/utils';

export default class Roll extends Command {
  constructor(client: CommandoClient) {
    super(client, {
      name: 'roll',
      group: 'roll',
      memberName: 'roll',
      description: 'Roll dice',
      guildOnly: true,
      args: [
        {
          key: 'dice',
          prompt: 'Какой кубик вы хотите бросить?',
          type: 'string',
          default: '1d100',
        },
      ],
    });
  }

  async run(msg: CommandMessage, { dice }: {dice: string}) {
    let count = 1;
    let sides = 100;

    let matches = dice.match(/^(\d+)d(\d+)$/);
    if (matches) {
      count = +matches[1];
      sides = +matches[2];
    } else {
      matches = dice.match(/^d?(\d+)$/);
      if (!matches) return msg.reply('Указано недопустимое выражение!');
      sides = +matches[1];
    }

    if (count < 1 || sides < 1) return msg.reply('Указано недопустимое выражение!');

    const results = [];
    for (let i = 0; i < count; i += 1) {
      results.push(Utils.getRandomInt(1, sides + 1));
    }

    const result = results.reduce((acc, val) => acc + val, 0);
    let message = `rolled **${result}**.`;
    if (count > 1) message += ` (${results.join(' + ')} = ${result})`;

    return msg.reply(message);
  }
}
