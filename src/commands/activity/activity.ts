import { CommandoClient, CommandMessage, Command } from 'discord.js-commando';
import { RichEmbed } from 'discord.js';
import Activity from '../../lib/activity';

export default class ActivityGet extends Command {
  constructor(client: CommandoClient) {
    super(client, {
      name: 'act',
      group: 'activity',
      memberName: 'act',
      description: 'Get activity statistics',
      guildOnly: true,
      args: [
        {
          key: 'member',
          prompt: 'По какому пользователю вы хотите посмотреть статистику?',
          type: 'string',
          default: '',
        },
      ],
    });
  }

  async run(msg: CommandMessage, { member }: {member: string}) {
    let userIds: string[];
    if (member) {
      userIds = [...member.matchAll(/<@!?(\d+)>/g)].map(m => m[1]);
      if (userIds.length === 0) {
        userIds = [msg.author.id];
      }
    }

    const activity = await Activity.getStatistics(userIds);
    const richEmbed = new RichEmbed();
    const message = activity.map(a => `\`${a.user}\`: ${a.messages}`).join('\n');
    richEmbed.addField('Статистика активности пользователей', message);
    return msg.say(richEmbed);
  }
}
