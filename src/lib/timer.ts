export default class Timer {
  private readonly time;

  constructor(time: number) {
    this.time = time;
  }

  getCountdown() {
    const distance = this.time - new Date().getTime();

    let days = 0;
    let hours = 0;
    let minutes = 0;
    // let seconds = 0;

    if (distance > 0) {
      days = Math.floor(distance / (1000 * 60 * 60 * 24));
      hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      // seconds = Math.floor((distance % (1000 * 60)) / 1000);
    }

    return `${days}д. ${Timer.pad(hours)}ч. ${Timer.pad(minutes)}м.`;
  }

  static pad(n: number) {
    return (`0${n}`).slice(-2);
  }

  toString() {
    return this.getCountdown();
  }
}
