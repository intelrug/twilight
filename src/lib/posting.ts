import { Client, RichEmbed, TextChannel } from 'discord.js';
import { CommandMessage } from 'discord.js-commando';
import { CronJob } from 'cron';
import { getLogger, Logger } from 'log4js';
import got from 'got';
import GuildEntity from '../entities/Guild';
import ChannelEntity from '../entities/Channel';

const logger: Logger = getLogger('POSTING');

export default class Posting {
  public static job: CronJob;

  public static async init(client: Client) {
    Posting.job = new CronJob('0 */5 * * * *', async () => {
      const channels: ChannelEntity[] = await ChannelEntity.find();
      // eslint-disable-next-line no-restricted-syntax
      for (let i = 0; i < channels.length; i += 1) {
        try {
          const response: any = await got(`https://derpibooru.org/api/v1/json/search/images?filter_id=${channels[i].filterId}&q=${channels[i].q}`).json();
          if (channels[i].lastPostId !== 0) {
            const images = response.images.filter(image => image.id > channels[i].lastPostId);
            for (let j = images.length - 1; j >= 0; j -= 1) {
              const textChannel: TextChannel = client.channels.get(channels[i].externalId) as TextChannel;
              try {
                await textChannel.send(`<https://derpibooru.org/${images[j].id}>`, {
                  files: [images[j].view_url],
                });
              } catch {
                try {
                  await textChannel.send(`https://derpibooru.org/${images[j].id}`);
                } catch (e2) {
                  logger.error(e2);
                }
              }
              channels[i].lastPostId = images[j].id;
              channels[i].save();
            }
          }
        } catch (e) {
          logger.error(e);
        }
      }
    }, null, true, 'Europe/Moscow', null, true);
  }

  public static async get(msg: CommandMessage) {
    const guild: GuildEntity = await GuildEntity.findOne({
      where: { externalId: msg.guild.id },
      relations: { channels: true },
    });
    if (guild) {
      if (guild.channels.length > 0) {
        const embed: RichEmbed = new RichEmbed();
        embed.setColor('PURPLE');
        embed.addField('Derpibooru Posting', guild.channels.map(c => `<#${c.externalId}>\n\`\`\`Filter: ${c.filterId}\`\`\` \`\`\`Query:\n\n${c.q.split(',').join(',\n')}\`\`\``));
        return msg.replyEmbed(embed);
      }
      return msg.reply('No channels for derpibooru posting found');
    }
    return null;
  }

  public static async set(msg: CommandMessage, channelId: string, value: string) {
    const channelEntity: ChannelEntity = await ChannelEntity.findOne({
      where: { externalId: channelId },
    });
    if (!channelEntity) {
      return msg.reply('You provided an invalid channel. Please try again.');
    }
    if (/^\d+$/.test(value)) {
      channelEntity.filterId = parseInt(value, 10);
    } else {
      channelEntity.q = value.replace(new RegExp('\\r?\\n', 'g'), '');
    }
    await channelEntity.save();
    return Posting.get(msg);
  }
}
