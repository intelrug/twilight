export default class Utils {
  static pluralize(count, words) {
    const cases = [2, 0, 1, 1, 1, 2];
    return words[(count % 100 > 4 && count % 100 < 20) ? 2 : cases[Math.min(count % 10, 5)]];
  }

  static getRandomInt(min: number, max: number) {
    const minimal = Math.ceil(min);
    const maximal = Math.floor(max);
    return Math.floor(Math.random() * (maximal - minimal)) + minimal;
  }

  static wait(ms: number) {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, ms)
    })
  }
}
