import axios, { AxiosResponse } from 'axios';
import { Client } from 'discord.js';
import { getLogger, Logger } from 'log4js';
import { IcecastStats } from '../types/IcecastStats';

const logger: Logger = getLogger('RADIO');

export default class Radio {
  static async getLiveInfo(client: Client) {
    try {
      const response: AxiosResponse<{
        icestats: IcecastStats
      }> = await axios.get('https://radio.lostfriendship.net/icecast-stats');
      if (!response.data || !response.data.icestats) return;
      const icecastStats = response.data.icestats;
      if (!icecastStats.source || !icecastStats.source[0] || !icecastStats.source[0].title) return;

      let title = icecastStats.source[0].title;
      const titleParts = title.split('-');
      const artist = titleParts.splice(0, 1);
      title = `${titleParts.join('-')} - ${artist}`;
      if (title.length > 90) {
        title = `${title.slice(0, 90)}...`;
      }
      title += '\nhttps://radio.lostfriendship.net';

      await client.user.setActivity(title, { type: 'LISTENING' });
    } catch (e) {
      logger.error(e);
    }
  }
}
