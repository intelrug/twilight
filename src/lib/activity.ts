import {
  ChannelLogsQueryOptions,
  Client,
  Collection,
  Guild,
  GuildChannel,
  GuildMember,
  Message, Role,
  Snowflake,
  TextChannel,
} from 'discord.js';
import { getLogger, Logger } from 'log4js';
import {
  getManager, If, In, LessThan, Raw,
} from 'typeorm';
import ActivityEntity from '../entities/Activity';
import Utils from './utils';

const logger: Logger = getLogger('ACTIVITY');

export default class Activity {
  private static guildId: string = process.env.GUILD_ID;
  private static maneRoleId: string = process.env.MANE_ROLE_ID;
  private static activeRoleId: string = process.env.ACTIVE_ROLE_ID;
  private static passiveRoleId: string = process.env.PASSIVE_ROLE_ID;
  private static newbieRoleId: string = process.env.NEWBIE_ROLE_ID;
  private static banRoleId: string = process.env.BAN_ROLE_ID;
  private static timeout: number = parseInt(process.env.ACTIVE_TIMEOUT, 10);
  private static resetTime: number = parseInt(process.env.RESET_TIME, 10);

  static async init(client: Client) {
    try {
      const guild: Guild = client.guilds.get(Activity.guildId);
      let messages: Message[] = [];
      let border: number = Date.now() - Activity.timeout;
      border = (border > this.resetTime || this.resetTime > Date.now()) ? border : this.resetTime;

      logger.info('Collecting new messages...');
      // eslint-disable-next-line no-restricted-syntax
      for (const channel of Activity.getReadableTextChannels(guild.channels)) {
        const channelsLastActivity: ActivityEntity = await Activity.getChannelsLastActivity(channel.id);
        if (channelsLastActivity && channelsLastActivity.timestamp < border) {
          messages = messages.concat(await Activity.getMessagesAfter(channel, channelsLastActivity.messageId));
        } else {
          messages = messages.concat(await Activity.getMessagesAfter(channel, border));
        }
        messages.sort((a, b) => a.createdTimestamp - b.createdTimestamp);
      }
      logger.info('Saving new messages...');

      const messagesIds: string[] = messages.map(m => m.id.toString());
      const activities: ActivityEntity[] = await ActivityEntity.find({
        where: { messageId: If(messagesIds.length > 0, In(messagesIds)) },
      });
      const newMessages: Message[] = messages.filter(m => !activities.find(a => a.messageId === m.id));

      await getManager().transaction(async transactionalEntityManager => {
        await Promise.all(newMessages.map(async a => {
          const activity: ActivityEntity = ActivityEntity.create({
            guildId: a.guild.id,
            channelId: a.channel.id,
            userId: a.author.id,
            user: a.author.tag,
            messageId: a.id,
            timestamp: a.createdTimestamp,
            content: a.cleanContent,
          });
          await transactionalEntityManager.save(activity);
        }));
        logger.info('All new messages were saved...');
      });
    } catch (e) {
      logger.error(e);
    }

    await Activity.check(client);
    setInterval(() => {
      Activity.check(client);
    }, 60 * 60 * 1000); // 1 hour
  }

  static getReadableTextChannels(channels: Collection<Snowflake, GuildChannel>): TextChannel[] {
    return Array.from(channels.values()).filter((((channel): channel is TextChannel => channel.type === 'text'
        && channel.memberPermissions(channel.guild.me)
          .has(['READ_MESSAGES', 'READ_MESSAGE_HISTORY']))));
  }

  static async getMessagesAfter(channel: TextChannel, messageId: string): Promise<Message[]>;
  static async getMessagesAfter(channel: TextChannel, timestamp: number): Promise<Message[]>;

  static async getMessagesAfter(channel: TextChannel, param: string | number): Promise<Message[]> {
    let messages: Message[] = [];
    const options: ChannelLogsQueryOptions = {
      limit: 100,
    };

    if (typeof param === 'string') {
      options.after = param;
    }

    let messagesPart: Collection<Snowflake, Message>;
    do {
      messagesPart = await channel.fetchMessages(options);

      if (messagesPart.size === 0) break;

      if (typeof param === 'string') {
        options.after = messagesPart.first().id;
        messages = Array.from(messagesPart.values()).concat(messages);
      } else {
        options.before = messagesPart.last().id;
        messages = messages.concat(Array.from(messagesPart.filter(m => m.createdTimestamp >= param).values()));
      }

      if (typeof param === 'number' && messagesPart.last().createdTimestamp < param) break;
    } while (messagesPart.size > 0);

    return messages;
  }

  static async getUsersActivity(guildId: string, userId: string): Promise<ActivityEntity[]> {
    return ActivityEntity.find({
      select: { timestamp: true },
      where: {
        userId,
        guildId,
        content: Raw(alias => `(CHAR_LENGTH(${alias}) >= 20 OR CHAR_LENGTH(${alias}) = 0 OR ${alias} IS NULL)`),
      },
    });
  }

  static async determineRole(member: GuildMember, activity: ActivityEntity[]): Promise<void> {
    const time = Date.now();
    let messages = activity.filter(({ timestamp }) => timestamp > time - 31 * 24 * 60 * 60 * 1000); // 31 days
    if (messages.length >= 500) {
      return Activity.setManeRole(member);
    }
    messages = activity.filter(({ timestamp }) => timestamp > time - 7 * 24 * 60 * 60 * 1000); // 7 days
    if (messages.length >= 3) {
      return Activity.setActiveRole(member);
    }
    return Activity.setPassiveRole(member);
  }

  static async getChannelsLastActivity(channelId: string): Promise<ActivityEntity> {
    return ActivityEntity.findOne({
      where: { channelId },
      order: { timestamp: 'DESC' },
    });
  }

  static async getStatistics(userIds: string[]): Promise<{user: string, messages: string}[]> {
    let activities = ActivityEntity.createQueryBuilder('a')
      .select('COUNT (*) as `messages`, `user`')
      .where('(CHAR_LENGTH(`content`) >= 20 OR CHAR_LENGTH(`content`) = 0 OR `content` IS NULL)')
      .andWhere(`timestamp > ${Date.now() - 31 * 24 * 60 * 60 * 1000}`);
    if (userIds && userIds.length > 0) {
      activities = activities.andWhere(`userId IN(${userIds.join(',')})`);
    }
    activities = activities.groupBy('userId')
      .orderBy('messages', 'DESC')
      .limit(30);
    return activities.execute();
  }

  static async check(client: Client) {
    try {
      await this.removeOldActivity();

      const { members } = await client.guilds.get(Activity.guildId).fetchMembers();
      const activities: {id: string, activity: ActivityEntity[]}[] = await getManager()
        .transaction(async transactionalEntityManager => Promise.all(
          members.map(async m => {
            const activity: ActivityEntity[] = await transactionalEntityManager.find(ActivityEntity, {
              select: { timestamp: true },
              where: {
                guildId: m.guild.id,
                userId: m.user.id,
                content: Raw(alias => `(CHAR_LENGTH(${alias}) >= 20 OR CHAR_LENGTH(${alias}) = 0 OR ${alias} IS NULL)`),
              },
            });
            return { id: m.user.id, activity };
          }),
        ));

      if (Date.now() >= this.resetTime && Date.now() - this.resetTime < 3 * 24 * 60 * 60 * 1000) {
        await Promise.all(members.map(async member => {
          await Utils.wait(3000);
          await Activity.setNewbieRole(member);
        }));
      } else {
        members.map(async member => {
          // eslint-disable-next-line eqeqeq
          const { activity } = activities.find(a => a.id == member.user.id);
          if (activity) {
            if (
              member.roles.has(Activity.newbieRoleId)
              && member.joinedTimestamp
              && Date.now() - member.joinedTimestamp >= 3 * 24 * 60 * 60 * 1000
              && Date.now() - this.resetTime >= 3 * 24 * 60 * 60 * 1000
            ) {
              // 3 days
              await Activity.removeNewbieRole(member);
            }
            await Utils.wait(1000);
            await Activity.determineRole(member, activity);
          }
        });
      }

      members.forEach(member => {
        if (member.roles.has(Activity.banRoleId)) {
          Activity.removeActivityRoles(member);
        }
      });
    } catch (e) {
      logger.error(e);
    }
  }

  static async removeOldActivity() {
    let border: number = Date.now() - Activity.timeout;
    border = (border > this.resetTime || this.resetTime > Date.now()) ? border : this.resetTime;
    const activities: ActivityEntity[] = await ActivityEntity.find({
      where: {
        timestamp: LessThan(border),
      },
    });
    await getManager().getRepository(ActivityEntity).remove(activities);
  }

  static async onMessage(message: Message) {
    try {
      if (message.guild && message.guild.id === Activity.guildId && message.type !== 'GUILD_MEMBER_JOIN') {
        await ActivityEntity.create({
          guildId: message.guild.id,
          channelId: message.channel.id,
          userId: message.author.id,
          user: message.author.tag,
          messageId: message.id,
          timestamp: message.createdTimestamp,
          content: message.cleanContent,
        }).save();
        const activity: ActivityEntity[] = await Activity.getUsersActivity(message.guild.id, message.author.id);
        await Activity.determineRole(message.member, activity);
      }
    } catch (e) {
      logger.error(e);
    }
  }

  static async onMessageDelete(message: Message) {
    try {
      if (message.guild && message.guild.id === Activity.guildId) {
        const activity: ActivityEntity = await ActivityEntity.findOne({
          where: { messageId: message.id },
        });
        if (activity) {
          await activity.remove();
        }
        const activities: ActivityEntity[] = await Activity.getUsersActivity(message.guild.id, message.author.id);
        await Activity.determineRole(message.member, activities);
      }
    } catch (e) {
      logger.error(e);
    }
  }

  static async onGuildMemberAdd(member: GuildMember) {
    await Activity.setNewbieRole(member);
  }

  static async onGuildMemberRemove(member: GuildMember) {
    const messages = [
      '%username% ушёл, хлопнув дверью напоследок.',
      'Всем известный %username% решил покинуть нас.',
      'Нас покинул %username%, мы будем помнить его, возможно.',
      '%username% откинулся.',
      'Мы не говорим прощай %username%, мы говорим до свидания.',
    ];
    const channel = member.guild.systemChannel as TextChannel;
    if (channel) {
      await channel.send(messages[Math.floor(Math.random() * messages.length)]
        .replace(/%username%/g, `${member.toString()} \`${member.user.username}#${member.user.discriminator}\``));
    }
  }

  static async onGuildMemberUpdate(oldMember: GuildMember, newMember: GuildMember) {
    if (newMember.roles.has(Activity.banRoleId)) {
      await Activity.removeActivityRoles(newMember);
    } else if (
      oldMember.roles.has(Activity.banRoleId)
      && !newMember.roles.has(Activity.banRoleId)
    ) {
      if (
        (!newMember.roles.has(Activity.newbieRoleId)
        && newMember.joinedTimestamp
        && Date.now() - newMember.joinedTimestamp < 3 * 24 * 60 * 60 * 1000)
        || (Date.now() >= this.resetTime && Date.now() - this.resetTime < 3 * 24 * 60 * 60 * 1000)
      ) {
        // 3 days
        await Activity.setNewbieRole(newMember);
      }
      const activities: ActivityEntity[] = await Activity.getUsersActivity(newMember.guild.id, newMember.user.id);
      await Activity.determineRole(newMember, activities);
    }
  }

  static async setManeRoleName(guild: Guild) {
    const role: Role = guild.roles.get(Activity.maneRoleId);
    const count = role.members.filter(({ id }) => id !== guild.client.user.id).size;
    await role.setName(`Mane ${count}`);
  }

  static async setNewbieRole(member: GuildMember) {
    try {
      if (
        !member.roles.has(Activity.newbieRoleId)
        && !member.roles.has(Activity.banRoleId)
      ) {
        await Activity.removeActivityRoles(member);
        await member.addRole(Activity.newbieRoleId);
      }
    } catch (e) {
      console.error(e.message);
    }
  }

  static async removeNewbieRole(member: GuildMember) {
    try {
      if (
        member.roles.has(Activity.newbieRoleId)
        && !member.roles.has(Activity.banRoleId)
      ) {
        await member.removeRole(Activity.newbieRoleId);
      }
    } catch (e) {
      console.error(e.message);
    }
  }

  static async setManeRole(member: GuildMember) {
    try {
      if (
        !member.roles.has(Activity.maneRoleId)
        && !member.roles.has(Activity.banRoleId)
        && !member.roles.has(Activity.newbieRoleId)
      ) {
        await member.addRole(Activity.maneRoleId);
        await member.removeRoles([Activity.activeRoleId, Activity.passiveRoleId]);
        await Activity.setManeRoleName(member.guild);
      }
    } catch (e) {
      console.error(e.message);
    }
  }

  static async setActiveRole(member: GuildMember) {
    try {
      if (
        !member.roles.has(Activity.activeRoleId)
        && !member.roles.has(Activity.banRoleId)
        && !member.roles.has(Activity.newbieRoleId)
      ) {
        await member.addRole(Activity.activeRoleId);
        await member.removeRoles([Activity.maneRoleId, Activity.passiveRoleId]);
        await Activity.setManeRoleName(member.guild);
      }
    } catch (e) {
      console.error(e.message);
    }
  }

  static async setPassiveRole(member: GuildMember) {
    try {
      if (
        !member.roles.has(Activity.passiveRoleId)
        && !member.roles.has(Activity.banRoleId)
        && !member.roles.has(Activity.newbieRoleId)
      ) {
        await member.addRole(Activity.passiveRoleId);
        await member.removeRoles([Activity.maneRoleId, Activity.activeRoleId]);
        await Activity.setManeRoleName(member.guild);
      }
    } catch (e) {
      console.error(e.message);
    }
  }

  static async removeActivityRoles(member: GuildMember) {
    try {
      await member.removeRoles([
        Activity.maneRoleId,
        Activity.activeRoleId,
        Activity.passiveRoleId,
        Activity.newbieRoleId,
      ]);
      await Activity.setManeRoleName(member.guild);
    } catch (e) {
      console.error(e.message);
    }
  }
}
