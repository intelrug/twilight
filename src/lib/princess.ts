import { GuildMember, Message } from 'discord.js';
import CommandTimeoutEntity from '../entities/CommandTimeout';
import Utils from './utils';

export default class PrincessCommand {
  static async run(message: Message) {
    const match = message.content.match(/([сз]дел[ао][йи] ?(и[зс])? +м[еи]ня +пр[ие]нц[еэ]с(с)?[оау][йи])|(х[оа]чу +стать? +пр[ие]нц[еэ]с(с)?[оау][йи])/i);
    if (!match) return;

    const member: GuildMember = message.guild.members.get(message.author.id);

    const inVoiceChannel: boolean = !!member.voiceChannelID;
    if (!inVoiceChannel) {
      await message.reply('Тебе необходимо зайти в голосовой канал, чтобы стать принцессой!');
      return;
    }

    if (member.voiceChannelID === '635592197667946543') {
      await message.reply('Ты уже являешься принцессой!');
      return;
    }

    const timeout: number = await PrincessCommand.getCommandTimeout('princess', member.user.id);
    if (timeout !== 0) {
      const minutes: number = Math.floor(timeout / (60 * 1000));
      const minutesWord: string = Utils.pluralize(minutes, ['минута', 'минуты', 'минут']);
      await message.reply(`В следующий раз ты сможешь стать принцессой через ${minutes} ${minutesWord}!`);
      return;
    }

    try {
      await member.setVoiceChannel('635592197667946543');
      await message.reply('Теперь ты принцесса');
      await PrincessCommand.resetCommandTimeout('princess', member.user.id);
    } catch (e) {
      await message.reply('У тебя не получилось стать принцессой');
    }
  }

  static async getCommandTimeout(command: string, userId: string) {
    let commandTimeout: CommandTimeoutEntity = await CommandTimeoutEntity.findOne({
      where: { command, userId },
    });
    if (!commandTimeout) {
      commandTimeout = await CommandTimeoutEntity.create({ command, userId }).save();
    }
    const left: number = 30 * 60 * 1000 - (Date.now() - parseInt(commandTimeout.usedAt, 10));
    return left >= 0 ? left : 0;
  }

  static async resetCommandTimeout(command: string, userId: string) {
    await CommandTimeoutEntity.update({
      command, userId,
    }, {
      usedAt: Date.now().toString(),
    });
  }
}
