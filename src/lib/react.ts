import {
  Client, GuildMember, Message, MessageReaction, TextChannel, User,
} from 'discord.js';
import ReactMessagesEntity from '../entities/ReactMessages';

export default class ReactMessages {
  private static list: ReactMessagesEntity[] = [];
  private static roles = [{
    emoji: '🔞',
    id: '677861261601538080',
  }, {
    emoji: 'pink5',
    emojiId: '677863894659760138',
    id: '677860837804736512',
  }];

  static async init(client: Client): Promise<void> {
    ReactMessages.list = await ReactMessagesEntity.find();
    ReactMessages.registerListeners(client);
  }

  static has(id: string): boolean {
    return !!ReactMessages.list.find(({ messageId }) => id === messageId);
  }

  static async create(message: Message): Promise<void> {
    // eslint-disable-next-line no-restricted-syntax
    for (const { emoji, emojiId } of ReactMessages.roles) {
      await message.react(emojiId || emoji);
    }
    await ReactMessagesEntity.create({
      messageId: message.id,
      channelId: message.channel.id,
    }).save();
    ReactMessages.list = await ReactMessagesEntity.find();
  }

  static async onReactionAdd(reaction: MessageReaction, user: User): Promise<void> {
    if (!reaction) return;
    const role = ReactMessages.roles.find(({ emoji, emojiId }) => emoji === reaction.emoji.name || emojiId === reaction.emoji.id);
    if (!role) return;
    const member: GuildMember = reaction.message.guild.members.get(user.id);
    if (!member) return;
    await member.addRole(role.id);
  }

  static async onReactionRemove(reaction: MessageReaction, user: User): Promise<void> {
    if (!reaction) return;
    const role = ReactMessages.roles.find(({ emoji, emojiId }) => emoji === reaction.emoji.name || emojiId === reaction.emoji.id);
    if (!role) return;
    const member: GuildMember = reaction.message.guild.members.get(user.id);
    if (!member) return;
    await member.removeRole(role.id);
  }

  static registerListeners(client: Client): void {
    client.on('raw', packet => {
      if (!['MESSAGE_REACTION_ADD', 'MESSAGE_REACTION_REMOVE'].includes(packet.t)) return;
      if (!ReactMessages.has(packet.d.message_id)) return;
      const channel = client.channels.get(packet.d.channel_id) as TextChannel;
      // There's no need to emit if the message is cached, because the event will fire anyway for that
      // if (channel.messages.has(packet.d.message_id)) return;
      channel.fetchMessage(packet.d.message_id).then(message => {
        const emoji = packet.d.emoji.id ? `${packet.d.emoji.name}:${packet.d.emoji.id}` : packet.d.emoji.name;
        const reaction = message.reactions.get(emoji);

        if (reaction) reaction.users.set(packet.d.user_id, client.users.get(packet.d.user_id));

        if (packet.t === 'MESSAGE_REACTION_ADD') {
          client.emit('messageReactionAdd', reaction, client.users.get(packet.d.user_id));
        }
        if (packet.t === 'MESSAGE_REACTION_REMOVE') {
          client.emit('messageReactionRemove', reaction, client.users.get(packet.d.user_id));
        }
      });
    });
  }
}
