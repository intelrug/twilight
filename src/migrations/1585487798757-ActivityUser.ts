import {MigrationInterface, QueryRunner} from "typeorm";

export class ActivityUser1585487798757 implements MigrationInterface {
    name = 'ActivityUser1585487798757'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `activity` ADD `user` varchar(255) NOT NULL", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `activity` DROP COLUMN `user`", undefined);
    }

}
