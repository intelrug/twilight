import {MigrationInterface, QueryRunner} from "typeorm";

export class AddActivityTable1573479310335 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `activity` (`id` int NOT NULL AUTO_INCREMENT, `guildId` varchar(255) NOT NULL, `channelId` varchar(255) NOT NULL, `messageId` varchar(255) NOT NULL, `userId` varchar(255) NOT NULL, `timestamp` bigint NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP TABLE `activity`");
    }

}
