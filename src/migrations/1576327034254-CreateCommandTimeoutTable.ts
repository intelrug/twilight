import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateCommandTimeoutTable1576327034254 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `command_timeouts` (`id` int NOT NULL AUTO_INCREMENT, `command` varchar(255) NOT NULL, `userId` bigint NOT NULL, `usedAt` bigint NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP TABLE `command_timeouts`");
    }

}
