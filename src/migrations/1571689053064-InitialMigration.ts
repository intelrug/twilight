import {MigrationInterface, QueryRunner} from "typeorm";

export class InitialMigration1571689053064 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `guilds` (`id` int NOT NULL AUTO_INCREMENT, `externalId` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `channels` (`id` int NOT NULL AUTO_INCREMENT, `externalId` varchar(255) NOT NULL, `guildId` int NOT NULL, `q` text NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `channels` ADD CONSTRAINT `FK_16f7ae247a7cf9894db7f23df8e` FOREIGN KEY (`guildId`) REFERENCES `guilds`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `channels` DROP FOREIGN KEY `FK_16f7ae247a7cf9894db7f23df8e`");
        await queryRunner.query("DROP TABLE `channels`");
        await queryRunner.query("DROP TABLE `guilds`");
    }

}
