import {MigrationInterface, QueryRunner} from "typeorm";

export class AddLastPostIdFieldInChannelTable1575290938994 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `channels` ADD `filterId` int NOT NULL DEFAULT 100073");
        await queryRunner.query("ALTER TABLE `channels` ADD `lastPostId` int NOT NULL DEFAULT 0");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `channels` DROP COLUMN `lastPostId`");
        await queryRunner.query("ALTER TABLE `channels` DROP COLUMN `filterId`");
    }

}
