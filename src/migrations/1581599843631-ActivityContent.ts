import {MigrationInterface, QueryRunner} from "typeorm";

export class ActivityContent1581599843631 implements MigrationInterface {
    name = 'ActivityContent1581599843631'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `activity` ADD `content` text NULL", undefined);
        await queryRunner.query("CREATE FULLTEXT INDEX `IDX_6b5f966d87f786f58044e0dbd2` ON `activity` (`content`)", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP INDEX `IDX_6b5f966d87f786f58044e0dbd2` ON `activity`", undefined);
        await queryRunner.query("ALTER TABLE `activity` DROP COLUMN `content`", undefined);
    }

}
