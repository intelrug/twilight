import {MigrationInterface, QueryRunner} from "typeorm";

export class ReactMessages1581692050233 implements MigrationInterface {
    name = 'ReactMessages1581692050233'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `react_messages` (`id` int NOT NULL AUTO_INCREMENT, `channelId` bigint NOT NULL, `messageId` bigint NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP TABLE `react_messages`", undefined);
    }

}
